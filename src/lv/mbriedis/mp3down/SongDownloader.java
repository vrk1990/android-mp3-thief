package lv.mbriedis.mp3down;

import android.app.DownloadManager;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import lv.mbriedis.mp3down.db.SongUrl;

public class SongDownloader {

    private SongUrl songUrl;
    private final Context context;
    private String filename;

    public SongDownloader(SongUrl songUrl, String filename, Context context) {
        this.songUrl = songUrl;
        this.context = context;
        this.filename = filename;
    }

    public void start() {
        DownloadManager.Request request = new DownloadManager.Request(songUrl.getUri());

        request.setDescription(songUrl.getUrl());
        request.setTitle(filename);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename + ".mp3");

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }
}
